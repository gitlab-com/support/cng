# p127520

Patch v15.11.1-ee's webservice container with
https://gitlab.com/gitlab-org/gitlab/-/merge_requests/127520

The `spec/` paths are removed, as the shipped image removes all `spec`
for the sake of efficiency.


## Build

docker build -f Dockerfile.gitlab-webservice-ee -t REPO/gitlab-webservice-ee:v15.11.1-p127520

